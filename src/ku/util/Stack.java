package ku.util;

import java.util.EmptyStackException;

/**
 * An Class of Stack that stack an element until its full.
 * @author Arut Thanomwatana
 *
 * @param <T> 
 */
public class Stack <T> 
{
	/** Capacity of an stack */
	private int capacity;
	/** array of item in the stack */
	private T[] items;
	/** Current index of the array*/
	private int index;
	
	/**
	 * Initialize a Stack.
	 * @param capacity is the max capacity that stack can contain
	 */
	public Stack(int capacity)
	{
		if(capacity<0)
			capacity = 0;
		this.capacity=capacity;
		index = 0;
		items = (T[]) new Object[capacity];
	}
	
	/**
	 * capacity is used for check max capacity in stack.
	 * @return capacity is max element that stack can contain
	 */
	public int capacity()
	{
		return this.capacity;
	}
	
	/**
	 * Check whether stack is empty or not.
	 * @return true if stack is empty
	 */
	
	public boolean isEmpty()
	{
		if(size()==0)
			return true;
		else 
			return false;
	}
	
	/**
	 * Check whether stack is full or not
	 * @return true if stack is full
	 */
	public boolean isFull()
	{
		if(size()==capacity)
			return true;
		else
			return false;
	}
	
	/**
	 * Check the top element in the stack
	 * @return T that is in the top of the stack 
	 */
	public T peek()
	{
		if(size()==0)
			return null;
		return items[index-1];
	}
	
	/**
	 * Take the element out of the top of the stack.
	 * @return T thia is in the top of the stack
	 */
	public T pop()
	{
		if(size()==0)
			throw new EmptyStackException();
		T obj = items[index-1];
		items[index-1] = null;
		index--;
		return obj;
	}
	
	/**
	 * Put the element in the top of the stack.
	 * @param obj is the T that will be put at the top
	 */
	public void push(T obj)
	{
		if(obj == null)
			throw new IllegalArgumentException();
		if(size()!=capacity)
			items[index++]=obj;
	}
	
	/**
	 * Check the number of the element in the stack.
	 * @return size is the number of the element in the stack.
	 */
	public int size()
	{
		int size = 0;
		for(int i =0;i<items.length;i++)
		{
			if(items[i]!=null)
				size++;
		}
		return size;
	}
	

}
