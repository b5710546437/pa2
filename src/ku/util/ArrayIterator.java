package ku.util;
import java.util.Iterator;
import java.util.NoSuchElementException;
/**
 * An ArrayIterator use for check an Array wheter if it still have next element or not. 
 * @author Arut Thanomwatana
 *
 * @param <T>
 */
public class ArrayIterator<T> implements Iterator<T>
{
	/** Array of a object */
	private T[] array;
	/** Current index of object*/
	private int index;
	/**
 	* Initialize the Array Iterator
 	* @param array of an object
 	*/
	public ArrayIterator(T[] array)
	{
		this.array = array;
		index = 0;
	}
	/**
	 * Next is used for inspect next Index
	 * @return Object T that is in next index
	 * @throws NoSuchElementException if there isn't any element left
	 */
	public T next()
	{
		if(hasNext())
		{
			return array[index++];
		}
		else
			throw new NoSuchElementException();
	}
	/**
	 * Check if there any next element in array
	 * @return true if there any next element
	 */
	public boolean hasNext()
	{
		while(index<array.length){
			if(array[index]==null)
			{
				index++;
			}
			else
				return true;
		}
		return false;
	}
	/**
	 * Remove the element that just call next
	 */
	public void remove()
	{
		array[index-1] = null;
	}

}
